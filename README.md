# Supply Zeppelin
## About
Supply Zeppelin is a bot that will drop crates with points if chat is active. The first one that clicks the following reaction will get the points.
Peoples can also check a per-guild scoreboard. For more help, check ;help.

I do host an instance which you can [add to your guild](https://discordapp.com/oauth2/authorize?client_id=317248850672746496&scope=bot&permissions=355392).
This runs on my private PC however and will not not be online 100%. It currently serves 25 servers (check with `;stats`).

## Programming
I for myself tried out actors with this bot. It may not be idiomatic in some places. Also, it currently uses MongoDB. All of this is subject to change. If you
want to run your own instance, create the table above manually. For running, you must provide as arguments (in oreder):
* The Bot Token from Discord
* The ID of the user able to use ;stop etc
* The ID of the crate emoji
* The password for the MongoDB user "supplyzeppelin"
* The collection for the MongoDB database
* The ID of the emoji triggering a ToS notice

Please also change ;help links and the ToS link before running publicly.

Contributions with merge requests and issues are welcome!
