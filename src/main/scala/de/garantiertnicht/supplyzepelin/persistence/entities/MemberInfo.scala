/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.entities

import de.garantiertnicht.supplyzepelin.Main
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.entities.Member

case class MemberInfo(guildId: GuildId, userId: UserId) {
  def getAsMember(jda: JDA = Main.jda): Option[Member] = Option(jda.getGuildById(guildId)).flatMap(guild => Option(guild.getMemberById(userId)))
}

object MemberInfo {
  implicit def info2member(memberInfo: MemberInfo): Option[Member] = memberInfo.getAsMember()
  implicit def member2info(member: Member): MemberInfo = apply(member)

  def apply(member: Member): MemberInfo = new MemberInfo(member.getGuild.getIdLong, member.getUser.getIdLong)
}
