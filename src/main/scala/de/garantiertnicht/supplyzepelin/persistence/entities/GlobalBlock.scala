package de.garantiertnicht.supplyzepelin.persistence.entities

case class GlobalBlock(_id: UserId, from: UserId, reason: String) {
  def user = userFromId(_id)
}
