/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.definitions

import de.garantiertnicht.supplyzepelin.persistence.Connection
import de.garantiertnicht.supplyzepelin.persistence.entities.{MemberInfo, ScoreboardEntry}
import net.dv8tion.jda.core.entities.Guild
import org.mongodb.scala.FindObservable
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._

class GetBlockedMembersForGuild(guild: Guild) extends MongoQuery[ScoreboardEntry] {
  override val mongoQuery: FindObservable[ScoreboardEntry] =
    Connection.scoreboard.find(
      and(equal("blocked", true), equal("_id.guildId", guild.getIdLong)))
}

class BlockUser(member: MemberInfo) extends MongoQuery[ScoreboardEntry] {
  override val mongoQuery = Connection.scoreboard
    .findOneAndUpdate(and(equal("_id", member), not(exists("blocked"))), set("blocked", true))
}

class UnBlockUser(member: MemberInfo) extends MongoQuery[ScoreboardEntry] {
  override val mongoQuery = Connection.scoreboard.findOneAndUpdate(and(equal("_id", member), equal("blocked", true)), unset("blocked"))
}
