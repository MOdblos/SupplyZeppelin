/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.definitions

import de.garantiertnicht.supplyzepelin.persistence.Connection
import de.garantiertnicht.supplyzepelin.persistence.entities.{GlobalBlock, UserId}
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.result.DeleteResult
import org.mongodb.scala.{Completed, FindObservable}

object GetGloballyBlockedMembers extends MongoQuery[GlobalBlock] {
  override val mongoQuery: FindObservable[GlobalBlock] = Connection.blocks.find()
}

case class GloballyBlockUser(block: GlobalBlock) extends MongoQuerySingle[Completed] {
  override val mongoQuery = Connection.blocks.insertOne(block)
}

case class GloballyUnBlockUser(user: UserId) extends MongoQuerySingle[DeleteResult] {
  override val mongoQuery = Connection.blocks.deleteOne(equal("_id", user))
}
