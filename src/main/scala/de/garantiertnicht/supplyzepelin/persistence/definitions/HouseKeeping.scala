/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.definitions

import com.mongodb.client.result.UpdateResult
import de.garantiertnicht.supplyzepelin.persistence.Connection
import de.garantiertnicht.supplyzepelin.persistence.entities.ScoreboardEntry
import net.dv8tion.jda.core.entities.Guild
import org.mongodb.scala.Observable
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.result.DeleteResult

case class DegradePoints(guild: Guild) extends MongoQuery[Unit] {
  override val mongoQuery: Observable[Unit] = Connection.scoreboard.find(and(equal("_id.guildId", guild.getIdLong), gt("score.score", 0))).map[Unit]((entry: ScoreboardEntry) ⇒ {
      Connection.scoreboard.updateOne(equal("_id", entry._id), set("score.score", (entry.score.score * 0.995).toLong)).subscribe((e: Throwable) => e.printStackTrace(), () ⇒ {})
    })
}

case class RemoveScore(guild: Guild) extends MongoQuery[UpdateResult] {
  override val mongoQuery: Observable[UpdateResult] = Connection.scoreboard.updateMany(and(equal("_id.guildId", guild.getIdLong), exists("score"), equal("score.score", 0), equal("score.static", 0)), unset("score"))
}

case class RemoveEmpty(guild: Guild) extends MongoQuery[DeleteResult] {
  override val mongoQuery: Observable[DeleteResult] = Connection.scoreboard.deleteMany(and(equal("_id.guildId", guild.getIdLong), not(exists("score")), not(exists("blocked")), not(exists("rewards"))))
}

