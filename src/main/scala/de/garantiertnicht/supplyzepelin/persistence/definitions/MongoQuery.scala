/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.definitions

import org.mongodb.scala.{Observable, SingleObservable}

import scala.language.implicitConversions

trait MongoQuery[A] {
  val mongoQuery: Observable[A]
}

trait MongoQuerySingle[A] extends MongoQuery[A] {
  val mongoQuery: SingleObservable[A]
}

object MongoQuery {
  implicit def q2q[A](mongoQuery: MongoQuery[A]): Observable[A] = mongoQuery.mongoQuery
  implicit def q2q[A](mongoQuery: MongoQuerySingle[A]): Observable[A] = mongoQuery.mongoQuery
}
