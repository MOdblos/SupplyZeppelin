/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin

import akka.actor.{ActorRef, ActorSystem, Props}
import de.garantiertnicht.supplyzepelin.bot._
import de.garantiertnicht.supplyzepelin.persistence.Connection
import de.garantiertnicht.supplyzepelin.persistence.entities.Points
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.MemberUpdate
import net.dv8tion.jda.core.entities.{Emote, Member}
import net.dv8tion.jda.core.events.ReadyEvent
import net.dv8tion.jda.core.events.guild.GuildJoinEvent
import net.dv8tion.jda.core.events.guild.member.{GuildMemberJoinEvent, GuildMemberRoleAddEvent, GuildMemberRoleRemoveEvent}
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.core.events.message.guild.react.{GuildMessageReactionAddEvent, GuildMessageReactionRemoveEvent}
import net.dv8tion.jda.core.hooks.ListenerAdapter
import net.dv8tion.jda.core.{AccountType, JDA, JDABuilder}

object Main {
  private val system = ActorSystem("supply-zeppelin")
  var bot: ActorRef = _
  var reaction: Emote = _
  var emojiId: Long = _
  var databasePassword: String = _
  var jda: JDA = _
  var superUser = -1L
  var databaseCollection: String = _
  val MAX_POINTS: Points = 1L << 62
  def maxPointsExceededMessage(member: Member) = s"Sorry, but you can not have more or equal then $MAX_POINTS, ${member.getAsMention}"
  val MAX_POINT_MODIFICATION: Points = 1L << 48
  def maxPointsModificationExceededMessage(member: Member) = s"Sorry, but you may not modify your points by an amount more or equal then $MAX_POINT_MODIFICATION points, ${member.getAsMention}"

  var tosEmote: Emote = _
  var tosEmoteId: String = _
  def tosClickEmote = s"To view the Terms of Service and Privacy Policy for bots from garantiertnicht software, please click ${tosEmote.getAsMention}."
  val tosNotice = "To view the Terms of Service and Privacy Policy for bots from garantiertnicht software: <https://discord.gg\\Ew3wyJj>."


  def main(args: Array[String]): Unit = {
    if(args.length < 3) {
      println("Missing Token, superuser id or emoji id")
      sys.exit(2)
    }

    val token = args(0)
    superUser = args(1).toLong
    emojiId = args(2).toLong
    databasePassword = args(3)
    databaseCollection = args(4)
    tosEmoteId = args(5)

    Connection.waitUntilReady()
    bot = system.actorOf(Props[Bot], "bot")

    jda = new JDABuilder(AccountType.BOT).setToken(token).addEventListener(Events).setEnableShutdownHook(false).buildAsync()
  }
}

object Events extends ListenerAdapter {
  override def onReady(event: ReadyEvent): Unit = {
    Main.reaction = event.getJDA.getEmoteById(Main.emojiId)
    Main.tosEmote = event.getJDA.getEmoteById(Main.tosEmoteId)
  }

  override def onGuildMessageReceived(event: GuildMessageReceivedEvent): Unit = {
    Main.bot ! GuildMessage(event.getMessage)
  }

  override def onGuildMessageReactionAdd(event: GuildMessageReactionAddEvent): Unit = {
    Main.bot ! GuildReaction(event.getReaction, event.getMember)
  }


  override def onGuildMessageReactionRemove(event: GuildMessageReactionRemoveEvent): Unit = {
    Main.bot ! GuildReactionRemoved(event.getReaction, event.getMember)
  }

  override def onGuildMemberJoin(event: GuildMemberJoinEvent) = Main.bot ! Recalculate(event.getGuild, MemberUpdate(), Seq(event.getMember))
  override def onGuildJoin(event: GuildJoinEvent) = Main.bot ! Recalculate(event.getGuild, MemberUpdate())
  override def onGuildMemberRoleAdd(event: GuildMemberRoleAddEvent) = Main.bot ! Recalculate(event.getGuild, MemberUpdate(), Seq(event.getMember))
  override def onGuildMemberRoleRemove(event: GuildMemberRoleRemoveEvent) = Main.bot ! Recalculate(event.getGuild, MemberUpdate(), Seq(event.getMember))
}
