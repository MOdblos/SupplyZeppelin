/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.parsing

import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.{Roles, ScoreAmount, _}
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.{Guild, Role}

class RewardParser(guild: Guild) extends GuildParser(guild) {
  val END: String = "\u0000"

  def score: Parser[ScoreAmount] = braced(opt(longConst) ~ opt(":" ~> longConst)) <~ "POINTS" ^^ {case (score ~ static) ⇒ ScoreAmount(score.getOrElse(0), static.getOrElse(0))}
  def rank: Parser[Rank] = "RANK" ~> intConst ^^ { Rank }
  def on: Parser[RewardSource] = "ON" ~> source ^^ { RewardSource }

  def noRoles: Parser[Seq[Option[Role]]] = "NONE" ^^ { _ ⇒ Seq() }

  def roles: Parser[Roles] = "ROLES" ~> braced("APPROVED" ~> (noRoles | rep1sep(role , ",") | failure("You need to specify at least one role or the `NONE' keyword."))) ~
    braced("DENIED" ~> (noRoles | rep1sep(role, ",") | failure("You need to specify at least one role or the `NONE' keyword."))) ^^ {
    case (approved ~ denied) ⇒
      Roles(approved.filter(_.isDefined).map(_.get.getIdLong), denied.filter(_.isDefined).map(_.get.getIdLong))
    }

  def rolesCheckPermissions: Parser[Roles] = if(guild.getSelfMember.hasPermission(Permission.MANAGE_ROLES)) {
    roles
  } else {
    failure("You cannot use this grant without granting the bot the `Manage Roles' permission.")
  }

  def redeem: Parser[Redeem] = "REDEEM" ^^ { _ => Redeem() }
  def transfer: Parser[PointTransfer] = "TRANSFER" ^^ { _ ⇒ PointTransfer() }
  def collect: Parser[Collect] = "COLLECT" ^^ { _ ⇒ Collect() }
  def negativeCrateDropped: Parser[Recurring] = "RECURRING" ^^ { _ ⇒ Recurring() }
  def bribed: Parser[Bribed] = "BRIBED" ^^ { _ ⇒ Bribed() }
  def rewardModified: Parser[RewardModified] = "REWARD_MODIFIED" ^^ { _ ⇒ RewardModified() }
  def memberUpdate: Parser[MemberUpdate] = "MEMBER_UPDATE" ^^ { _ ⇒ MemberUpdate() }
  def nothingEvent: Parser[Nothing] = "EVERYTHING" ^^ { _ ⇒ Nothing() }

  def grant: Parser[RewardEntity] = braced(score | rolesCheckPermissions |
    failure("You need to insert at least one grant here. Grants are: SCORE, ROLES"))
  def nonLogicalRequirement: Parser[RewardEntity] = braced(score | roles | rank | on |
    failure("You need to insert at least one requirement here. Requirements are: SCORE, ROLES, RANK, ON. Requirements" +
      " can also be combined with AND and/or OR."))
  def logicalRequirement(i: Int): Parser[RewardEntity] = if(i == 0) nonLogicalRequirement else nonLogicalRequirement | and(i) | or(i)
  def source: Parser[Event] = braced(redeem | transfer | collect | negativeCrateDropped | bribed | nothingEvent |
    failure("You need to insert one event here. Events are: REDEEM, TRANSFER, COLLECT, RECURRING, BRIBED, REWARD_MODIFIED, MEMBER_UPDATE, EVERYTHING"))
  def and(i: Int): Parser[And] = rep1sep(logicalRequirement(i - 1), "AND") ^^ { And }
  def or(i: Int): Parser[Or] = rep1sep(logicalRequirement(i - 1), "OR") ^^ { Or }

  def nothing: Parser[Seq[RewardEntity]] = "UNCHANGED" ^^ { _ ⇒ Seq() }
  def optionalRewardEntities(entityParser: Parser[RewardEntity]): Parser[Seq[RewardEntity]] =
    rep1sep(entityParser, ",") | nothing | failure("You need to specify at least one entity or specify the `UNCHANGED' keyword.") ^^ {
      reward: (Seq[RewardEntity]) ⇒ reward
    }

  def rewardData: Parser[RewardData] = (braced("REQUIRES" ~> rep1sep(logicalRequirement(8), ",")) | failure("You need to " +
    "specify at least one requirement.")) ~
    (braced("GRANTS" ~> rep1sep(grant, ",")) | failure("You need to specify at least one grant.")) ^^ {
    case (requirement ~ grant) ⇒
      RewardData(requirement, grant)
  }

  def rewardDataOptional: Parser[RewardData] = (("REQUIRES" | failure("You need to specify the REQUIRES keyword. The list of requirements can be replaced with `UNCHANGED'."))
      ~> optionalRewardEntities(logicalRequirement(8))) ~
    (("GRANTS" | failure("You need to specify the `GRANTS' keyword. The list of grants can be replaced with `UNCHANGED'."))
      ~> optionalRewardEntities(grant)) ^^ {
    case (requirement ~ grant) ⇒
      RewardData(requirement, grant)
  }

  def optionPriority(options: RewardOptions): Parser[RewardOptions] = "PRIORITY" ~> intConst ^^ { value ⇒ options.copy(optPriority = Some(value)) }
  def optionPermanent(options: RewardOptions): Parser[RewardOptions] = "PERMANENT" ^^ { (_) ⇒ options.copy(optRemove = Some(1)) }
  def optionNonPermanent(options: RewardOptions): Parser[RewardOptions] = "LOSABLE" ^^ { (_) ⇒ options.copy(optRemove = Some(0)) }
  def optionNoPersist(options: RewardOptions): Parser[RewardOptions] = "SINGLE" ^^ { (_) ⇒ options.copy(optRemove = Some(-1)) }

  def option(options: RewardOptions): Parser[RewardOptions] = braced(optionPriority(options) | optionPermanent(options) | optionNonPermanent(options) | optionNoPersist(options) |
    failure("You must insert a valid option here. Options are: PRIORITY <int>, PERMANENT, LOSABLE, SINGLE"))

  def reward(optionsInstance: RewardOptions = RewardOptions.default): Parser[Reward] = stringConst ~ rewardData ~ opt("SET" ~> rep1sep(option(optionsInstance), ","))  ^^ {
    case (name ~ data ~ options) ⇒
      createReward(name, data, options, optionsInstance)
  }

  def rewardDataIsOptional(optionsInstance: RewardOptions): Parser[Reward] = stringConst ~ braced(rewardDataOptional) ~ opt("SET" ~> rep1sep(option(optionsInstance), ",")) ^^ {
    case (name ~ data ~ options) ⇒
      createReward(name, data, options, optionsInstance)
  }

  def createReward(name: String, data: RewardData, options: Option[Seq[RewardOptions]], optionsInstance: RewardOptions): Reward = {
    val optionsToUse: Option[RewardOptions] = if(options.isDefined) {
      val effective = options.get.reduce{ (o, n) ⇒
        if(n.optRemove.nonEmpty) o.copy(optRemove = n.optRemove)
        else if(n.optPriority.nonEmpty) o.copy(optPriority = n.optPriority)
        else o
      }

      if(effective != optionsInstance) {
        Some(effective)
      } else {
        None
      }
    } else {
      None
    }

    Reward(RewardIdentifier(guild.getIdLong, name), data, optionsToUse)
  }

  def oneReward(optionsInstance: RewardOptions = RewardOptions.default): Parser[Reward] = reward(optionsInstance) <~ END
  def oneRewardOptionalData(optionsInstance: RewardOptions = RewardOptions.default): Parser[Reward] = rewardDataIsOptional(optionsInstance) <~ END
}
