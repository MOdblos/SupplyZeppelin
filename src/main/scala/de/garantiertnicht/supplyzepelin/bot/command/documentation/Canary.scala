/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2018 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.documentation

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.bot.deployment.{Deployment, Rewards}
import net.dv8tion.jda.core.entities.Message

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.Duration

object Canary extends Command {
  override val name: String = "canary"
  val deployments: Seq[Deployment] = Seq(Rewards)

  override def execute(commandAndArgs: Array[String], originalMessage: Message, guild: ActorRef)(implicit ex: ExecutionContext): Unit = {
    originalMessage.getChannel.sendMessage(deployments.map { deployment ⇒
      val name = deployment.getClass.getSimpleName.replace("$", "")
      val servers = Main.jda.getGuilds.asScala.count(deployment.isEligible)
      val thisServerEligible = deployment.isEligible(originalMessage.getGuild)
      val thisServerEligibleString = if(thisServerEligible) {
        "available"
      } else {
        deployment.eligibleAt(originalMessage.getGuild) match {
          case None ⇒ "coming soon™"
          case Some(Duration.Inf) ⇒
            "not available at the moment"
          case Some(duration) ⇒
            val hours = duration.toHours
            val minutes = duration.toMinutes - hours * 60
            val seconds = duration.toSeconds - (hours * 60 * 60 + minutes * 60)

            s"available in approx. $hours hours, $minutes minutes and $seconds seconds"
        }

      }
      "Feature \"" + name + '"' + s" is $thisServerEligibleString, already deployed on $servers guilds."
    }.mkString("```", "\n", "```")).queue()
  }
}
