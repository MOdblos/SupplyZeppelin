/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.parsing

import net.dv8tion.jda.core.entities._

import scala.collection.JavaConverters._

class GuildParser(guild: Guild) extends BasicParser {
  def role: Parser[Option[Role]] = roleId | roleMention | roleName | failure("A valid role (id, mention or name) is expected here.")
  def roleMention: Parser[Option[Role]] = "<@&" ~> roleId <~ ">"
  def roleId: Parser[Option[Role]] = longConst ^^ { id => Option(guild
    .getRoleById(id))
  }
  def roleName: Parser[Option[Role]] = stringConst ^^ { name => {
    val list = guild.getRolesByName(name, true)
    if (list.size != 1) {
      None
    } else {
      Option(list.get(0))
    }
  }
  }

  def user: Parser[Option[User]] = member ^^ {
    _
      .map(_
        .getUser)
  }
  def member: Parser[Option[Member]] = memberId | memberMention | memberName
  def memberMention: Parser[Option[Member]] = "<@!?"
    .r ~> memberId <~ ">"
  def memberId: Parser[Option[Member]] = longConst ^^ { id => Option(guild
    .getMemberById(id))
  }
  def memberName: Parser[Option[Member]] = stringConst ~ ("#" ~> "[0-9]{4}") ^^ { case name ~ discriminator => {
    val list = guild
      .getMembersByName(name, true)
      .asScala
      .filter(_
        .getUser
        .getDiscriminator == discriminator)
    if (list
      .length != 1) {
      None
    } else {
      list
        .headOption
    }
  }
  }

  def channel: Parser[Option[TextChannel]] = channelId | channelMention | channelName
  def channelMention: Parser[Option[TextChannel]] = "<@#" ~> channelId <~ ">"
  def channelId: Parser[Option[TextChannel]] = longConst ^^ { id => Option(guild
    .getTextChannelById(id))
  }
  def channelName: Parser[Option[TextChannel]] = stringConst ^^ { name => {
    val list = guild
      .getTextChannelsByName(name, true)
    if (list
      .size != 1) {
      None
    } else {
      Option(list
        .get(0))
    }
  }
  }
}
