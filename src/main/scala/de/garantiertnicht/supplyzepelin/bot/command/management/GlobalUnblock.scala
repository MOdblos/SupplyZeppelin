package de.garantiertnicht.supplyzepelin.bot.command.management

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.GloballyUnblock
import de.garantiertnicht.supplyzepelin.bot.command.Command
import net.dv8tion.jda.core.entities.Message

import scala.concurrent.ExecutionContext

object GlobalUnblock extends Command {
  override val name: String = "downgrade"

  override def execute
  (commandAndArgs: Array[String], originalMessage: Message, guild: ActorRef)(implicit ex: ExecutionContext): Unit = {
    if (originalMessage.getAuthor.getIdLong != Main.superUser) {
      return
    }

    originalMessage.getMentionedUsers.forEach(user => Main.bot ! GloballyUnblock(user))
    originalMessage.getChannel.sendMessage(
      "Sorry, but you are no longer eligible for Supply Boat and have been refunded."
    ).queue()
  }
}
