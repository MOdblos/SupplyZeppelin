package de.garantiertnicht.supplyzepelin.bot.command.management

import akka.actor.ActorRef
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.{Bot, GloballyBlock}
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.persistence.entities
import net.dv8tion.jda.core.entities.{Message, User}

import scala.concurrent.ExecutionContext

object GlobalBlock extends Command {
  override val name: String = "upgrade"

  override def execute
  (commandAndArgs: Array[String], originalMessage: Message, guild: ActorRef)(implicit ex: ExecutionContext): Unit = {
    if (originalMessage.getAuthor.getIdLong != Main.superUser) {
      return
    }

    val reasonOrEmpty = commandAndArgs.slice(1, commandAndArgs.length).filterNot(_.startsWith("<@")).mkString(" ")
    val reason = if (reasonOrEmpty.isEmpty) {
      "being an awesome space alien"
    } else {
      reasonOrEmpty
    }

    originalMessage.getMentionedUsers.forEach(user => {
      if (!Bot.isBlocked(user)) {
        Main.bot ! GloballyBlock(entities.GlobalBlock(
          user.getIdLong,
          originalMessage.getAuthor.getIdLong,
          reason
        ))

        originalMessage.getChannel.sendMessage(getMessage(user, reason)).queue()
      }
    })
  }

  def getMessage(user: User, reason: String): String = {
    s"""Thank you for using garantiertnicht software. Your account ${user.getAsMention} has been upgraded to Supply Boat! You are given the following privileges for $reason:
         |
         |* Never again will your commands spam the chat uselessly! Advanced algorithms clean it up for you.
         |* Advanced protection against unwanted transfers! Never again recieve transfers you didn't want
         |* Special crate emoji! You can now (only) click the spcial Supply Boat emoji to get twice the points!
         |
         |If you have further questions, you may want to watch our Supply Boat informercial at <https://youtu.be/FXPKJUE86d0>
         |We are also glad to help you with the special Supply Boat support at https://discord.gg\\Ew3wyJj""".stripMargin
  }
}
