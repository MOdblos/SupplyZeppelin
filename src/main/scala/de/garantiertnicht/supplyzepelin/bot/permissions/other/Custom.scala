/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.permissions.other

import de.garantiertnicht.supplyzepelin.bot.permissions.Permission

class Custom(value: Long) extends Permission(64 - value) {
  override val default = false
  override val name = "CUSTOM " + value
  override val description = "Custom Permission for own usage"
}

object Custom {
  val custom1 = new Custom(1)
  val custom2 = new Custom(2)
  val custom3 = new Custom(3)
  val custom4 = new Custom(4)
  val custom5 = new Custom(5)
  val custom6 = new Custom(6)
  val custom7 = new Custom(7)
  val custom8 = new Custom(8)
  val custom9 = new Custom(9)
  val custom10 = new Custom(10)
  val custom11 = new Custom(11)
  val custom12 = new Custom(12)
  val custom13 = new Custom(13)
  val custom14 = new Custom(14)
  val custom15 = new Custom(15)
  val custom16 = new Custom(16)
  val custom17 = new Custom(17)
  val custom18 = new Custom(18)
  val custom19 = new Custom(19)
  val custom20 = new Custom(20)
  val custom21 = new Custom(21)
  val custom22 = new Custom(22)
  val custom23 = new Custom(23)
  val custom24 = new Custom(24)
}
