/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot

import java.io.{BufferedOutputStream, File, FileOutputStream, OutputStream}
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef, PoisonPill, Props, SupervisorStrategy}
import akka.event.Logging
import akka.routing.{DefaultResizer, SmallestMailboxPool}
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.command.{CommandExecution, CommandManager}
import de.garantiertnicht.supplyzepelin.persistence.definitions.{GetGloballyBlockedMembers, GloballyBlockUser, GloballyUnBlockUser}
import de.garantiertnicht.supplyzepelin.persistence.entities.GlobalBlock
import net.dv8tion.jda.core.entities._

import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.ExecutionException
import scala.util.{Failure, Success}

case class GuildCommand(message: Message, command: String)
case class GuildMessage(message: Message)
case class GuildReaction(reaction: MessageReaction, author: Member)
case class GuildReactionRemoved(reaction: MessageReaction, author: Member)
case class GloballyBlock(block: GlobalBlock)
case class GloballyUnblock(user: User)
case class IsGloballyBlocked(user: User)

case class CrateCollectionStatistic(
  timeInMilliseconds: Long,
  ping: Long,
  first: Boolean
)

object StopSystem
object ReloadAllGuilds
object ClearGuildMap

class Bot extends Actor {
  val BIG_MASTER_SWITCH = false

  val prefix = ";"
  val log = Logging(context.system, this)

  val crateCollectionCsv = new FileOutputStream("collections.csv", true)

  val commandResizer = DefaultResizer(lowerBound = 1, upperBound = 5)
  val commandDispatcher = context.actorOf(SmallestMailboxPool(1, Some(commandResizer)).props(Props(classOf[CommandManager], Seq
  (
    command.documentation.Help,
    command.fun.Bribe,
    command.management.Block,
    command.management.Unblock,
    command.management.Reward,
    command.points.Points,
    command.points.Scoreboard,
    command.fun.Transfer,
    command.fun.Redeem,
    command.system.Stop,
    command.system.DebugInfo,
    command.system.Stats,
    command.documentation.Canary,
    command.system.DropCrateCommand,
    command.fun.Rewards,
    command.system.ReloadGuilds,
    command.management.GlobalBlock,
    command.management.GlobalUnblock,
    command.system.Eval
  ))), "command-dispatcher")

  val rewardResizer = DefaultResizer(lowerBound = 2, upperBound = 10)
  val rewardActor = context.actorOf(SmallestMailboxPool(1, Some(rewardResizer)).props(Props(classOf[RewardActor])))

  var shouldStop = false
  var shouldTerminate = false
  implicit val dispatcher = context.system.dispatcher

  GetGloballyBlockedMembers.mongoQuery.toFuture().onComplete({
    case Success(blockedMembers: Seq[GlobalBlock]) =>
      Bot.globalBlocks = blockedMembers.toBuffer
    case _ => // Something went wrong, but we don't care that much
      log.warning("Could not load blocked members")
  })


  def processInGuild(guild: Guild, message: Any): Unit = {
    context.actorSelection(guild.getId).resolveOne(1 second).onComplete {
      case Success(actor: ActorRef) => actor ! message
      case Failure(exception) =>
        if(!shouldStop && (!BIG_MASTER_SWITCH || guild.getIdLong == 204155906638872576L)) {
          try {
            val props = Props(classOf[GuildActor], guild)
            val actor = context.actorOf(props, guild.getId)

            actor ! message
          } catch {
            case e: Exception =>
              log.warning(s"Guild ${guild.getName} (${guild.getId}) failed to load:")
              e.printStackTrace()
          }
        }
    }
  }

  override def postStop(): Unit = {
    crateCollectionCsv.close()
    super.postStop()
  }

  override def receive: Receive = {
    case message: GuildMessage =>
      if(!message.message.getAuthor.isBot) {
        val content = message.message.getRawContent
        if(content.startsWith(prefix)) {
          processInGuild(message.message.getGuild, GuildCommand(message.message, content.substring(prefix.length)))
        } else if(content.nonEmpty && content.charAt(0).isLetter) {
          processInGuild(message.message.getGuild, message)
        }
      } else {
        processInGuild(message.message.getGuild, GuildBotMessage(message.message))
      }
    case recalculate: Recalculate ⇒ rewardActor.forward(recalculate)
    case reaction: GuildReaction =>
      if(!reaction.author.getUser.isBot) {
        processInGuild(reaction.reaction.getGuild, reaction)
      }
    case GuildReactionRemoved(reaction, author) ⇒
      if (reaction.getEmote.getEmote == Main.tosEmote) {
        processInGuild(reaction.getGuild, GuildReaction(reaction, author))
      }
    case command: CommandExecution => commandDispatcher ! command
    case CrateCollectionStatistic(time, ping, first) =>
      crateCollectionCsv.write(s"$time, $ping, $first\n".getBytes)
    case remove: RemoveRewardFromMembers ⇒ rewardActor.forward(remove)
    case StopSystem =>
      log.info("Shutting down…")
      shouldStop = true
      shouldTerminate = true
      context.actorSelection("*") ! GuildActorShouldBePoisoned

      context.system.scheduler.scheduleOnce(
        FiniteDuration(320, TimeUnit.SECONDS),
        self,
        PoisonPill
      )
    case ReloadAllGuilds ⇒
      context.actorSelection("*") ! GuildActorShouldBePoisoned
      shouldStop = true
      shouldTerminate = false
      context.system.scheduler.scheduleOnce(
        FiniteDuration(5, TimeUnit.MINUTES),
        self,
        ClearGuildMap
      )
    case ClearGuildMap ⇒
      shouldStop = false
      shouldTerminate = false
    case GloballyBlock(block) =>
      GloballyBlockUser(block).mongoQuery.head()
      Bot.globalBlocks.append(block)
    case GloballyUnblock(user) =>
      GloballyUnBlockUser(user.getIdLong).mongoQuery.head()
      Bot.globalBlocks.find(_._id == user.getIdLong).foreach(
        block => Bot.globalBlocks.remove(Bot.globalBlocks.indexWhere(_._id == user.getIdLong))
      )
  }

  override def supervisorStrategy: SupervisorStrategy = SupervisorStrategy.stoppingStrategy
}

object Bot {
  private var globalBlocks = mutable.Buffer[GlobalBlock]()

  def isBlocked(user: User): Boolean = {
    globalBlocks.exists(_._id == user.getIdLong)
  }
}