/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.deployment
import net.dv8tion.jda.core.entities.Guild

import scala.concurrent.duration.Duration

trait Capped extends Canary {
  val noAfter: Long

  override def isEligible(guild: Guild): Boolean = {
    System.currentTimeMillis() <= noAfter && super.isEligible(guild)
  }

  override def eligibleAt(guild: Guild): Option[Duration] = {
    if(System.currentTimeMillis() > noAfter) {
      Some(Duration.Inf)
    } else {
      super.eligibleAt(guild)
    }
  }
}
